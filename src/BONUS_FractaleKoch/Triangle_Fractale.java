package BONUS_FractaleKoch;
// Triangle de Koch fait pour montrer la comprehension du fonctionnement de la creation de fractale recursive
import java.awt.Dimension;
import java.awt.*;
import java.awt.geom.Line2D;
import java.util.Scanner;
import java.awt.Graphics;
import javax.swing.JFrame;
import javax.swing.JPanel;

/*@author No� Breton
 * 
 * */
//classe sans lib exterieur a Swing et awt, utilise Graphics2D, plus precis que Graphics car il utilise des doubles
public class Triangle_Fractale extends JFrame {

	JPanel p = new JPanel();

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// initialise la fenetre graphique
	Triangle_Fractale() {
		setSize(new Dimension(750, 800));
		p = new JPanel();
		setContentPane(p);
		setVisible(true);
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	// partie dessin
	// ######################################"
	static int n; // ordre de la fractal

	// "paint" le dessin sur le panel
	public void paint(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
		FractaleTriangle(g2, 500 / Math.pow(3, n), n);
		// diviser le pas permet de compenser legerement le changement de taille du au
		// changement d'ordre

	}

	private double x = 125, y = 300; // coordonn�e du point de depart
	 float angle = 0; // angle de depart

// affiche la fractale en carr� en tournant l'angle et en generant une ligne fractale a chaque iteration
	public void FractaleTriangle(Graphics2D g, double pas, int n) {
		for (int i = 0; i < 3; i++) {
			FractaleLigne(g, pas, n);
			angle += 120;
		}
		angle += 180;

	}

	// trace une ligne de la fractale demand�e
	/*
	 * Graphics2D g = permet de tracer des formes Double pas = longeur du trait qui
	 * va etre tracer ou l'avanc�e de la ligne qui est trac� int n = ordre de la
	 * fractale
	 * 
	 */
	public void FractaleLigne(Graphics2D g, double pas, int n) {
		if (n == 0) {
			double x0 = x;
			double y0 = y;
			x += pas * Math.cos(Math.toRadians(angle));
			y += pas * Math.sin(Math.toRadians(angle));
			g.draw(new Line2D.Double(x0, y0, x, y)); // trace une ligne avec des coordon�e Double bas� sur le pas
		} else {
			FractaleLigne(g, pas, n - 1);
			angle -= 60; //rotation
			FractaleLigne(g, pas, n - 1);
			angle += 120;
			FractaleLigne(g, pas, n - 1);
			angle -= 60;
			FractaleLigne(g, pas, n - 1);



			
			// la ligne est trac�, on change d'angle a 90 degrees
			/*FractaleLigne(g, pas, n - 1);
			angle -= 90;
			FractaleLigne(g, pas, n - 1);
			angle -= 90;
			FractaleLigne(g, pas, n - 1);
			angle += 90;
			FractaleLigne(g, 2 * pas, n - 1);*/
		}
	}

	// ################################
//fin partie dessin
	public static void main(String[] args) {
		System.out.print("entrez l'ordre, Fractale d'ordre n = ");
		Scanner sc = new Scanner(System.in);
		n = sc.nextInt(); // selectionne l'ordre, essayer n=5 par exemple
		new Triangle_Fractale();
		sc.close();

	}
}
